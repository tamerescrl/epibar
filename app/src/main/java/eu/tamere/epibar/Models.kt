package eu.tamere.epibar

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


@Entity
class Bar(@PrimaryKey(autoGenerate = true) val uid: Int,
          val name: String,
          val address: String) {}
