package eu.tamere.epibar

import android.app.IntentService
import android.content.Intent
import android.util.Log


class BarGeneratorService() : IntentService("BarGeneratorThreadName") {
    override fun onDestroy() {
        super.onDestroy()
        Log.d("BarGeneratorService", "onDestroy()")
    }

    override fun onCreate() {
        super.onCreate()
        Log.d("BarGeneratorService", "onCreate()")
    }

    override fun onHandleIntent(intent: Intent?) {
        Log.d("BarGeneratorService", "onHandleIntent()")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d("BarGeneratorService", "onStartCommand()")
        return super.onStartCommand(intent, flags, startId)
    }
}
