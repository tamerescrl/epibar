package eu.tamere.epibar


import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.transition.Fade
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.TextView


class BarAdapter(val presenter: MainContracts.Presenter): RecyclerView.Adapter<BarViewHolder>() {

    override fun getItemCount(): Int {
        return presenter.numberOfBars
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): BarViewHolder {
        val barView = LayoutInflater
                .from(parent?.context)
                .inflate(android.R.layout.simple_list_item_1, parent, false)

        val handler: (Int) -> Unit = { position: Int ->
            val tappedBar = presenter.barAt(position)
            Log.d("BarAdapter", "Bar tapped ${tappedBar.name}")
            presenter.barTapped(tappedBar)
        }

        return BarViewHolder(barView, handler)
    }

    override fun onBindViewHolder(holder: BarViewHolder?, position: Int) {
        val bar = presenter.barAt(position)
        holder?.barName?.text = "${bar.name} ${bar.uid}"
    }
}

class BarViewHolder(itemView: View?, val tapHandler: ((Int) -> Unit)) : ViewHolder(itemView) {
    val barName: TextView?

    init {
        barName = itemView?.findViewById(android.R.id.text1)
        itemView?.setOnClickListener { tapHandler.invoke(adapterPosition) }
    }
}

class MainActivity : AppCompatActivity(), MainContracts.View, MainContracts.Platform {

    companion object {
        val TAG = "MainActivity"
    }

    lateinit var presenter: MainContracts.Presenter
    lateinit var barsList: RecyclerView
    lateinit var adapter: BarAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = MainPresenter(App.db.barDao(), this, this)
        adapter = BarAdapter(presenter)

        barsList = findViewById(R.id.barsList)
        barsList.setAdapter(adapter)
        barsList.setLayoutManager(LinearLayoutManager(this))
        // Exercise: Add a decoration

        val startGeneratorButton: Button = findViewById(R.id.startGenerator)
        val stopGeneratorButton: Button = findViewById(R.id.stopGenerator)

        startGeneratorButton.setOnClickListener { presenter.startGenerator() }
        stopGeneratorButton.setOnClickListener { presenter.stopGenerator() }

        Log.d(TAG, "onCreated() finished")
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
        // If you return false, the menu wont be displayed
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_add_bar -> {
                Log.d("MainActivity", "Add bar was tapped!")
                presenter.generateNewBar()
                return true // Means that we've consumed the event
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun refreshList() {
        adapter.notifyDataSetChanged()
    }

    override fun displayBar(barToDisplay: Bar) {
        val detailsIntent = Intent(this, BarDetailsActivity::class.java)
        detailsIntent.putExtra("BAR_UID", barToDisplay.uid)
        startActivity(detailsIntent)
    }

    override fun startLongRunningGenerator() {
        val intent = Intent(this, BarGeneratorService::class.java)
        startService(intent)
    }

    override fun stopLongRunningGenerator() {
        val intent = Intent(this, BarGeneratorService::class.java)
       stopService(intent)
    }
}
