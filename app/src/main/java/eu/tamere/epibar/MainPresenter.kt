package eu.tamere.epibar


class MainPresenter(val barDao: BarDao,
                    val view: MainContracts.View,
                    val platform: MainContracts.Platform): MainContracts.Presenter {

    override fun startGenerator() {
        platform.startLongRunningGenerator()
    }

    override fun stopGenerator() {
        platform.stopLongRunningGenerator()
    }

    override fun barTapped(tappedBar: Bar) {
        // Maybe do something with the bar?
        // Like updating a counter on it?
        view.displayBar(tappedBar)
    }

    override fun generateNewBar() {
        barDao.insert(Bar(0,"New Bar", "Brussels"))
        view.refreshList()
    }

    override val numberOfBars: Int
        get() = barDao.getAll().size

    override fun barAt(position: Int): Bar {
        return barDao.getAll()[position]
    }
}
