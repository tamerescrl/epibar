package eu.tamere.epibar


class MainContracts {

    interface Platform {
        fun startLongRunningGenerator()
        fun stopLongRunningGenerator()
    }

    interface View {
        fun refreshList()
        fun displayBar(barToDisplay: Bar)
    }

    interface Presenter {
        val numberOfBars: Int
        fun barAt(position: Int): Bar
        fun generateNewBar()
        fun barTapped(tappedBar: Bar)
        fun startGenerator()
        fun stopGenerator()
    }
}
