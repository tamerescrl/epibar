package eu.tamere.epibar

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface BarDao {
    @Query("SELECT * FROM bar")
    fun getAll(): List<Bar>

    @Query("SELECT * FROM bar WHERE uid = :barId LIMIT 1")
    fun findById(barId: Int): Bar

    @Insert
    fun insert(barToInsert: Bar)
}
