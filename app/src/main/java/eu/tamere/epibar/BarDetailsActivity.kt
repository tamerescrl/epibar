package eu.tamere.epibar

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView

class BarDetailsActivity : AppCompatActivity() {
    lateinit var barName: TextView
    lateinit var barAddress: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bar_details)

        barName = findViewById(R.id.barName)
        barAddress = findViewById(R.id.barAddress)

        if(intent.hasExtra("BAR_UID")) {
            val barIdToRetrieve = intent.getIntExtra("BAR_UID", 0)
            val bar = App.db.barDao().findById(barIdToRetrieve)

            barName.text = bar.name
            barAddress.text = bar.address
        } else {
            barName.text = "Unknown"
            barAddress.text = "Unknown"
        }

        setupToolBar()
    }

    fun setupToolBar() {
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setTitle(barName.text)
        }
    }
}
