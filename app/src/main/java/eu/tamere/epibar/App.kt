package eu.tamere.epibar

import android.app.Application
import android.arch.persistence.room.Room


class App: Application() {
    companion object {
        lateinit var db: AppDatabase
    }

    override fun onCreate() {
        super.onCreate()
        setupRoom()
    }

    private fun setupRoom() {
        db = Room.databaseBuilder(this, AppDatabase::class.java, "EpiBar.db")
                .allowMainThreadQueries()
                .build()
    }
}
