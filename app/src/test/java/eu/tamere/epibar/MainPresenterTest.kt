package eu.tamere.epibar


import org.junit.Assert.assertEquals
import org.junit.Test


class MainPresenterTest {

    val defaultView =  object : MainContracts.View {
        override fun displayBar(barToDisplay: Bar) {}
        override fun refreshList() {}
    }

    @Test
    fun testInitialState() {
        val presenter = MainPresenter(arrayListOf(), defaultView)
        assertEquals(0, presenter.numberOfBars)
    }

    @Test
    fun testGenerateBar() {
        val presenter = MainPresenter(arrayListOf(), defaultView)
        presenter.generateNewBar()

        assertEquals(1, presenter.numberOfBars)
    }
}
